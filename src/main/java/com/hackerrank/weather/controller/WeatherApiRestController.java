package com.hackerrank.weather.controller;

import com.hackerrank.weather.model.Weather;
import com.hackerrank.weather.model.WeatherSort;
import com.hackerrank.weather.repository.WeatherRepository;
import com.hackerrank.weather.search.MatchType;
import com.hackerrank.weather.search.SearchCriteria;
import com.hackerrank.weather.search.SearchOperation;
import com.hackerrank.weather.search.SearchSpecification;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.provider.HibernateUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/weather")
public class WeatherApiRestController {

    private final WeatherRepository weatherRepository;

    public WeatherApiRestController(WeatherRepository weatherRepository) {
        this.weatherRepository = weatherRepository;
    }

    @PostMapping
    public ResponseEntity<Weather> create(@RequestBody Weather weather) {
        Weather save = weatherRepository.save(weather);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<Weather>> get(@RequestParam(required = false) List<String> city,
                                             @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
                                             @RequestParam(required = false) String sort) {

        List<Weather> all = weatherRepository.findAll(getWeatherSpecification(city, date, sort)
        );
        return new ResponseEntity<>(all, HttpStatus.OK);
    }

    private Specification<Weather> getWeatherSpecification(List<String> city, Date date, String sort) {
        return (root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (city != null) {
                city.replaceAll(String::toUpperCase);
                predicates.add(builder.upper(root.get("city")).in(city));
            }
            if (date != null) {
                predicates.add(builder.and(builder.equal(root.get("date"), date)));
            }
            if (sort != null) {
                WeatherSort ordering = WeatherSort.findByValue(sort);
                if (ordering == WeatherSort.DATE_ASC) {
                    query.orderBy(builder.asc(root.get("date")));
                } else if (ordering == WeatherSort.DATE_DESC) {
                    query.orderBy(builder.desc(root.get("date")));
                }
            }

            Predicate[] predicates1 = predicates.stream()
                    .toArray(Predicate[]::new);

            return builder.and(predicates1);
        };
    }

    @GetMapping("/{id}")
    public ResponseEntity<Weather> get(@PathVariable Integer id) {
        Optional<Weather> byId = weatherRepository.findById(id);
        if (byId.isPresent()) {
            return ResponseEntity.ok(byId.get());
        }

        return ResponseEntity.notFound().build();
    }


    private SearchCriteria searchCriteria(String key, Object value) {
        return SearchCriteria
                .builder()
                .key(key)
                .value(value)
                .operation(SearchOperation.MATCH)
                .build();
    }

}
