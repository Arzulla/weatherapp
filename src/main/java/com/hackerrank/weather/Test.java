package com.hackerrank.weather;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        System.out.println("test");
        List<Integer> weight = new ArrayList<>();
        weight.add(1);
        weight.add(3);
        weight.add(6);

        int maxIndex = weight.size();
        int startIndex = 0;
        int nextIndex = 1;

        int possibleSegment = 0;

        while (startIndex <= maxIndex-1) {
            System.out.println(getDifference(weight.subList(startIndex, nextIndex+1)));
            if (getDifference(weight.subList(startIndex, nextIndex+1)) <= 3) {
                possibleSegment++;
            }
            nextIndex++;
            if (nextIndex > maxIndex) {
                startIndex++;
                nextIndex=startIndex+1;
            }
        }

        //   System.out.println(possibleSegment);


    }

    public static long getDifference(List<Integer> weights) {
        int min = weights.get(0);
        int max = weights.get(0);
        for (Integer i : weights) {
            if (i > max) {
                max = i;
            } else if (i < min) {
                min = i;
            }
        }
        return max - min;
    }

    public static int getIteration(int n) {

        return (n + 1) / 2 * n;
    }
}
