package com.hackerrank.weather.model;

public enum WeatherSort {
    DATE_ASC("date"), DATE_DESC("-date"),UNKNOWN("UNKNOWN");

    private final String ordering;

    WeatherSort(String order) {
        this.ordering = order;
    }

    public String getOrdering() {
        return this.ordering;
    }

    public static WeatherSort findByValue(String message) {
        for (WeatherSort val : values()) {
            if (val.getOrdering().equalsIgnoreCase(message)) {
                return val;
            }
        }
        return WeatherSort.UNKNOWN;
    }
}
