package com.hackerrank.weather.repository;

import com.hackerrank.weather.model.Weather;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.Date;
import java.util.List;


public interface WeatherRepository extends JpaRepository<Weather, Integer>, JpaSpecificationExecutor<Weather> {

    @Query("select  t from Weather  t where (UPPER(t.city) in :cities or :cities is null ) " +
            "and " +
            "(:date is null  or t.date=:date) order by t.id asc ")
    List<Weather> foo(@Param("cities") List<String> cities, @Param("date") Date date);
}
