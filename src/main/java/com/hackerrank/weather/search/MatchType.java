package com.hackerrank.weather.search;

public enum MatchType {
    AND, OR
}
